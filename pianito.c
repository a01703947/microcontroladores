#include "custom/io.h"
#define F_CPU 4000000 // 4MhZ

#define DO_4 261.6
#define RE_4 293.6
#define MI_4 329.6
#define FA_4 349.2
#define SOL_4 391.9
#define LA_4 440.0
#define SI_4 493.8
#define DO_5 523.2

void play(float freq) {
  float count;
  count = F_CPU/freq;
  unsigned int int_count = count; 
  float diff = count - int_count;
  if (diff >= 0.5)
    int_count++;
  OCR1A = int_count;
  TCCR1A = 0x40; // Set to CTC with toggle
  TCCR1B = 0x09; // Set to CTC with CK
}
void stop() {
  TCCR1A = 0;
  TCCR1B = 0;
  set(PORTB, 1, 0); // Reset OC1A
}
void play_note(unsigned int button, float freq) {
  if (!poll(PIND, button)) {
    play(freq);
    while(!poll(PIND, button));
  }
}
void main() {
  // SET BUTTONS
  DDRB = 2; // Set PB1 as output (buzzer)
  PORTD = 0xFF; // Set PD0 - PD7 as pull-up input

  while (1) {
    play_note(0, DO_4);
    play_note(1, RE_4);
    play_note(2, MI_4);
    play_note(3, FA_4);
    play_note(4, SOL_4);
    play_note(5, LA_4);
    play_note(6, SI_4);
    play_note(7, DO_5);
    stop();
  }

}
