#define F_CPU 4000000 // 4 MhZ
#include <stdio.h>
#include <stdlib.h>
#include "custom/io.h"
#include <avr/interrupt.h>
#include <util/delay.h>

#define LCD_COMMS_DELAY 40
#define LCD_E_PORT PORTD
#define LCD_RS_PORT PORTD
#define LCD_E PD3
#define LCD_RS PD2
#define LCD_4_PORT PORTD
#define LCD_5_PORT PORTD
#define LCD_6_PORT PORTD
#define LCD_7_PORT PORTD
#define LCD_4 PD4
#define LCD_5 PD5
#define LCD_6 PD6
#define LCD_7 PD7
#include "lcd.h"

#define TIMER_COUNT 45535

unsigned int buzzer_timer = 0;
ISR(TIMER1_OVF_vect) { // Interruption caused by Timer 1
    PORTC ^= (1<<PC1); // Toggle value
    TCNT1 = TIMER_COUNT;
    buzzer_timer++;
}

void main () {
  DDRD = 0xFC; // Set PD2-PD7 to output
  DDRC = 0x02; // Set PC1 as output
  PORTC = 0x01; // Set PC0 as pull-up input

  TIMSK1 |= (1<<TOIE1); // Enable Timer 1 Overflow interruption
  sei(); // Enable global interrupts

  TCNT0 = 0; // Initialize the count to 0
  TCCR0B = (1<<CS00); // Set CK/1

  // INITIALIZE THE LCD
  init_4bit_lcd(2);
  clear_display();
  set_display(1,0,0);

  unsigned int random_int;
  unsigned char bottom_buffer[17];

  while (1) {
    // 400 MHz = 2 * 200 MHz
    // 200 MHz => 0.5ms
    // 0.2s/0.5ms = 40 iterations
    if (buzzer_timer >= 40) {
      set(TCCR1B, CS00, 0); // Turn off the timer and buzzer
      low(PINC, PC1);
      buzzer_timer = 0;
    }
    if (!poll(PINC, PC0)) { // If the button is pressed
      _delay_us(20); // Bounce-back delay
      while(!poll(PINC, PC0)); // Wait for its release
      _delay_us(20); // Bounce-back delay

      /* MAKING THE BUZZER SOUND */
      TCNT1 = TIMER_COUNT; // Reset the counter
      set(TCCR1B, CS00, 1); // Turn on counter with CK/1

      srand(TCNT0);
      random_int = rand() % 1000;
    }
    sprintf(
        bottom_buffer,
        "      %03i       ",
        random_int
    );
    go_home();
    write_str(" Random Numbers ");
    go_to(1,0);
    write_str(bottom_buffer);
  }
}
