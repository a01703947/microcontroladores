#define F_CPU 4000000 // 4 MhZ
#include <avr/io.h>
#include <avr/interrupt.h>

unsigned int COUNTER = 54899; // 47 Hz

ISR(TIMER1_OVF_vect) {
  // Resetear el counter
  PORTB ^= (1<<PB0); // Togglear un 1
  TCNT1 = COUNTER;
}

int main() {
  // Set port as output
  DDRB = (1<<PB0);

  // Habilitar el overflow
  TIMSK1 |= (1<<TOIE1);
  // Enable global interrupts
  sei();
  // Set the value of the counter
  TCNT1 = COUNTER;
  TCCR1B = (1<<CS10);  // CK


  while (1) {}
}
