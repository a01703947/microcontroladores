#include "custom/io.h"
#include <stdio.h>
#define F_CPU 4000000UL // 4 MhZ'

/* ---------- CABLEADO --------------
 *
 *              ----MCU---
 *             |          | PC4->INICIO
 * LCD_7<-PD3  |          |
 * LCD_6<-PD2  |     A    |
 * LCD_5<-PD1  |     T    | PC1->LCD_E
 * LCD_4<-PD0  |     m    | PC0->LCD_RS
 *             |     e    |
 *             |     g    |
 *             |     a    |
 *             |     3    |
 *             |     2    |
 *             |     8    |
 * SUB_PP<-PD5 |     p    |
 * RES_PP<-PD6 |          |
 * ADD_PP<-PD7 |          |
 *             |          |
 *              ----------
 * ---------------------------------- */
#define LCD_COMMS_DELAY 40
#define LCD_E_PORT PORTC
#define LCD_RS_PORT PORTC
#define LCD_E PC1
#define LCD_RS PC0
#define LCD_7_PORT PORTD
#define LCD_6_PORT PORTD
#define LCD_5_PORT PORTD
#define LCD_4_PORT PORTD
#define LCD_7 PD3
#define LCD_6 PD2
#define LCD_5 PD1
#define LCD_4 PD0
#include "lcd.h"

unsigned int people = 0;
unsigned char bot_buffer[17];

void main() {
  /* SETUP */
  DDRD = 0x0F;
  DDRC = 0x03;

  PORTD = 0xE0; // PD7, PD6, PD5 pull-up
  
  // INITIALIZE THE LCD
  init_4bit_lcd(2);
  clear_display();
  set_display(1,0,0);


  while (1) {
    if (!poll(PIND, PD5)) {
      while(!poll(PIND, PD5));
      if (people) people--;
    }
    if (!poll(PIND, PD6)) {
      while(!poll(PIND, PD6));
      people = 0;
    }
    if (!poll(PIND, PD7)) {
      while(!poll(PIND, PD7));
      people = (people + 1) % 1000;
    }
    sprintf(bot_buffer, "      %03i       ", people);
    go_home();
    write_str(" People Counter ");
    go_to(1,0);
    write_str(bot_buffer);
  }
}
