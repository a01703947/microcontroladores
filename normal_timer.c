#define F_CPU 4000000 // 4 MhZ
#include <avr/io.h>
#include <avr/interrupt.h>

unsigned int COUNTER = 120; // 57 Hz

ISR(TIMER0_OVF_vect) {
  // Resetear el counter
  PORTB ^= (1<<PB0); // Mandar un 1
  TCNT0 = COUNTER;
}

int main() {
  // Set port as output
  DDRB = (1<<PB0);

  // Habilitar el overflow
  TIMSK0 |= (1<<TOIE0);
  // Enable global interrupts
  sei();
  // Set the value of the counter
  TCNT0 = COUNTER;
  TCCR0B = (1<<CS01) | (1<<CS00);  // CK/64


  while (1) {}
}
