#include "custom/io.h"
#define F_CPU 4000000 // 4MhZ
#include <util/delay.h>

const int du =262,
    re= 294,
    ri=312,
    mi =330,
    fa=349,
    fi=370,
    sol=391,
    si=416,
    la=440,
    li=467,
    ti=494;
int MarioBros[591] = {
  mi*2, mi*2, 1, mi*2, 1, du*2, mi*2, 1, sol*2, 1, 1, 1, sol,
  1, 1, 1, du*2, 1, 1, sol, 1, 1, mi, 1, 1, la, 1, ti, 1, li, la, 1, sol, mi*2,
  1, sol*2, la*2, 1, fa*2, sol*2, 1, mi*2, 1, du*2, re*2, ti, 1, 1, du*2, 1, 1,
  sol, 1, 1, mi, 1, 1, la, 1, ti, 1, li, la, 1, sol, mi*2, 1, sol*2, la*2, 1,
  fa*2, sol*2, 1, mi*2, 1, du*2, re*2, ti, 1, 1, 1, 1, sol*2, fi*2, fa*2, ri*2,
  1, mi*2, 1, si, la, du*2, 1, la, du*2, re*2, 1, 1, sol*2, fi*2, fa*2, ri*2,
  1, mi*2, 1, du*4, 1, du*4, du*4, 1, 1, 1, 1, 1, sol*2, fi*2, fa*2, ri*2, 1,
  mi*2, 1, si, la, du*2, 1, la, du*2, re*2, 1, 1, ri*2, 1, 1, re*2, 1, 1, du*2,
  1, 1, 1, 1, 1, 1, 1, 1, 1, sol*2, fi*2, fa*2, ri*2, 1, mi*2, 1, si, la, du*2,
  1, la, du*2, re*2, 1, 1, sol*2, fi*2, fa*2, ri*2, 1, mi*2, 1, du*4, 1, du*4,
  du*4, 1, 1, 1, 1, 1, sol*2, fi*2, fa*2, ri*2, 1, mi*2, 1, si, la, du*2, 1,
  la, du*2, re*2, 1, 1, ri*2, 1, 1, re*2, 1, 1, du*2, 1, 1, 1, 1, 1, 1, 1,
  du*2, du*2, 1, du*2, 1, du*2, re*2, 1, mi*2, du*2, 1, la, sol, 1, 1, 1, du*2,
  du*2, 1, du*2, 1, du*2, re*2, 1, 1, 1, 1, 1, 1, 1, 1, du*2, du*2, 1, du*2, 1,
  du*2, re*2, 1, mi*2, du*2, 1, la, sol, 1, 1, 1, mi*2, mi*2, 1, mi*2, 1, du*2,
  mi*2, 1, sol*2, 1, 1, 1, sol, 1, 1, 1, du*2, 1, 1, sol, 1, 1, mi, 1, 1, la,
  1, ti, 1, li, la, 1, sol, mi*2, 1, sol*2, la*2, 1, fa*2, sol*2, 1, mi*2, 1,
  du*2, re*2, ti, 1, 1, du*2, 1, 1, sol, 1, 1, mi, 1, 1, la, 1, ti, 1, li, la,
  1, sol, mi*2, 1, sol*2, la*2, 1, fa*2, sol*2, 1, mi*2, 1, du*2, re*2, ti, 1,
  1, mi*2, du*2, 1, sol, 1, 1, si, 1, la, fa*2, 1, fa*2, la, 1, 1, 1, ti, la*2,
  1, la*2, la*2, sol*2, 1, fa*2, mi*2, du*2, 1, la, sol, 1, 1, 1, mi*2, du*2,
  1, sol, 1, 1, si, 1, la, fa*2, 1, fa*2, la, 1, 1, 1, ti, fa*2, 1, fa*2, fa*2,
  mi*2, 1, re*2, sol, mi, 1, mi, du, 1, 1, 1, mi*2, du*2, 1, sol, 1, 1, si, 1,
  la, fa*2, 1, fa*2, la, 1, 1, 1, ti, la*2, 1, la*2, la*2, sol*2, 1, fa*2,
  mi*2, du*2, 1, la, sol, 1, 1, 1, mi*2, du*2, 1, sol, 1, 1, si, 1, la, fa*2,
  1, fa*2, la, 1, 1, 1, ti, fa*2, 1, fa*2, fa*2, mi*2, 1, re*2, sol, mi, 1, mi,
  du, 1, 1, 1, du*2, du*2, 1, du*2, 1, du*2, re*2, 1, mi*2, du*2, 1, la, sol,
  1, 1, 1, du*2, du*2, 1, du*2, 1, du*2, re*2, 1, 1, 1, 1, 1, 1, 1, 1, du*2,
  du*2, 1, du*2, 1, du*2, re*2, 1, mi*2, du*2, 1, la, sol, 1, 1, 1, mi*2, mi*2,
  1, mi*2, 1, du*2, mi*2, 1, sol*2, 1, 1, 1, sol, 1, 1, 1, mi*2, du*2, 1, sol,
  1, 1, si, 1, la, fa*2, 1, fa*2, la, 1, 1, 1, ti, la*2, 1, la*2, la*2, sol*2,
  1, fa*2, mi*2, du*2, 1, la, sol, 1, 1, 1, mi*2, du*2, 1, sol, 1, 1, si, 1,
  la, fa*2, 1, fa*2, la, 1, 1, 1, ti, fa*2, 1, fa*2, fa*2, mi*2, 1, re*2, sol,
  mi, 1, mi, du, 1, 1, 1, 0
};

void play(float freq) {
  float count;
  count = F_CPU/freq;
  unsigned int int_count = count; 
  float diff = count - int_count;
  if (diff >= 0.5)
    int_count++;
  OCR1A = int_count;
  TCCR1A = 0x40; // Set to CTC with toggle
  TCCR1B = 0x09; // Set to CTC with CK
}
void stop() {
  TCCR1A = 0;
  TCCR1B = 0;
  set(PORTB, 1, 0); // Reset OC1A
}
void main() {
  // SET BUTTONS
  DDRB = 2; // Set PB1 as output (buzzer)
  PORTD = 0xFF; // Set PD0 - PD7 as pull-up input

  while (1) {
    for (unsigned int i = 0; i < 591; i++) {
      if (MarioBros[i] != 1) {
        play(MarioBros[i]);
        _delay_ms(100);
        stop();
      } else {
        _delay_ms(100);
      }
    }
  }

}
