#include "custom/io.h"
#include <stdio.h>
// Configurar velocidad del reloj
#define F_CPU 4000000UL
/* ---------- CABLEADO --------------
 *
 *              ----MCU---
 *             |          |
 * LCD_7<-PD3  |          |
 * LCD_6<-PD2  |     A    |
 * LCD_5<-PD1  |     T    | PC1->LCD_E
 * LCD_4<-PD0  |     m    | PC0->LCD_RS
 *             |     e    |
 *             |     g    |
 *             |     a    |
 *             |     3    |
 *             |     2    |
 *             |     8    |
 * RESET <-PD5 |     p    |
 * TOGGLE<-PD6 |          |
 * BUTTON<-PD7 |          |
 *             |          |
 *              ----------
 * ---------------------------------- */
#define LCD_E_PORT PORTC
#define LCD_RS_PORT PORTC
#define LCD_E PC1
#define LCD_RS PC0
#define LCD_7_PORT PORTD
#define LCD_6_PORT PORTD
#define LCD_5_PORT PORTD
#define LCD_4_PORT PORTD
#define LCD_7 PD3
#define LCD_6 PD2
#define LCD_5 PD1
#define LCD_4 PD0

#include "lcd.h"
void main () {
  // Pins & Ports Setup
  DDRD = 0x0F; // OUTPUT
  DDRC = 0x03; // OUTPUT

  PORTD = 0xE0; // Set the pull-ups

  // Set custom characters
  unsigned int custom_char_0[8] = {
    0b01110,
    0b01110,
    0b10100,
    0b11111,
    0b00101,
    0b10100,
    0b01010,
    0b00001
  };
  unsigned int custom_char_1[8] = {
    0b01110,
    0b01110,
    0b00101,
    0b11111,
    0b10100,
    0b00100,
    0b01010,
    0b01010
  };
  set_custom_char(0x08, custom_char_0);
  set_custom_char(0x10, custom_char_1);

  // Initialize the LCD
  init_4bit_lcd(2);
  clear_display();
  set_display(1,0,0);

  // Variables
  char buffer[16];
  unsigned int seconds = 0;
  unsigned int minutes = 0;
  unsigned int hours = 0;
  unsigned int delay_counter = 0;
  unsigned int running = 1;


  void animate(unsigned int seconds) {
    go_to(1,0); // 24 * 120 -> 2880us
    switch (seconds % 16) {
      case 0:
        write_str("\x01               "); // 640us
        break;
      case 1:
        write_str(" \x02              "); // 640us
        break;
      case 2:
        write_str("  \x01             "); // 640us
        break;
      case 3:
        write_str("   \x02            "); // 640us
        break;
      case 4:
        write_str("    \x01           "); // 640us
        break;
      case 5:
        write_str("     \x02          "); // 640us
        break;
      case 6:
        write_str("      \x01         "); // 640us
        break;
      case 7:
        write_str("       \x02        "); // 640us
        break;
      case 8:
        write_str("        \x01       "); // 640us
        break;
      case 9:
        write_str("         \x02      "); // 640us
        break;
      case 10:
        write_str("          \x01     "); // 640us
        break;
      case 11:
        write_str("           \x02    "); // 640us
        break;
      case 12:
        write_str("            \x01   "); // 640us
        break;
      case 13:
        write_str("             \x02  "); // 640us
        break;
      case 14:
        write_str("              \x01 "); // 640us
        break;
      case 15:
        write_str("               \x02"); // 640us
        break;
    }
  }

  while (1) {
    if (running) {
      sprintf(buffer, "    %02i:%02i:%02i    ", hours, minutes, seconds);
      go_home(); // 5ms = 5000 us
      write_str(buffer); // 16 * 40us = 640us
      animate(seconds); // 3.52ms = 3520 us
      delay_counter = 0;
      seconds++;
      if (seconds == 60) {
        minutes++;
        seconds = 0;
      }
      if (minutes == 60) {
        hours++;
        minutes = 0;
      }
      if (hours == 99) {
        hours = 0;
      }
      /*
       *  1000000us
       * -   5000us //go_home
       * -    640us //write_str
       * -   3520us // animate
       * ----------
       *   992720us
       *  /  1077us
       * __________
       *    920
       */
      for(; delay_counter < 920; delay_counter++) {
        // Reiniciar el cronometro
        if (!((PIND >> PD5) & 0x01)) {
          seconds = 0;
          minutes = 0;
          seconds = 0;
          break;
        }
        // Detener/empezar el cronometro
        if (!((PIND >> PD6) & 0x01)) {
          while(!((PIND >> PD6) & 0x01)) _delay_us(200); // El delay del bounce & debounce
          running = 0;
          // Decrementamos el counter por el delay que nos saltamos
          if (delay_counter > 0) {
            delay_counter--; 
          }
          break;
        }
        _delay_us(1077);
      }
    } else {
      if (!((PIND >> PD6) & 0x01)) {
        while(!((PIND >> PD6) & 0x01)) _delay_us(200); // El delay del bounce & debounce
        running = 1;
      }
      // Reiniciar el cronometro
      if (!((PIND >> PD5) & 0x01)) {
        while(!((PIND >> PD5) & 0x01)) _delay_us(200); // El delay del bounce & debounce
        seconds = 0;
        minutes = 0;
        seconds = 0;
        sprintf(buffer, "    %02i:%02i:%02i    ", hours, minutes, seconds);
        go_home(); // 5ms = 5000 us
        write_str(buffer); // 16 * 40us = 640us
      }
    }
  }
}
