#include "custom/io.h"
#include <stdio.h>
#define F_CPU 4000000UL
/* ---------- CABLEADO --------------
 *
 *              ----MCU---
 *             |          | PC4->INICIO
 * LCD_7<-PD3  |          |
 * LCD_6<-PD2  |     A    |
 * LCD_5<-PD1  |     T    | PC1->LCD_E
 * LCD_4<-PD0  |     m    | PC0->LCD_RS
 *             |     e    |
 *             |     g    |
 *             |     a    |
 *             |     3    |
 *             |     2    |
 *             |     8    |
 * A_ROCK<-PD5 |     p    |
 * A_PAPR<-PD6 |          |
 * A_SCIS<-PD7 |          | PB2->B_SCIS
 * B_ROCK<-PB0 |          | PB1->B_PAPR
 *              ----------
 * ---------------------------------- */
#define LCD_COMMS_DELAY 40
#define LCD_E_PORT PORTC
#define LCD_RS_PORT PORTC
#define LCD_E PC1
#define LCD_RS PC0
#define LCD_7_PORT PORTD
#define LCD_6_PORT PORTD
#define LCD_5_PORT PORTD
#define LCD_4_PORT PORTD
#define LCD_7 PD3
#define LCD_6 PD2
#define LCD_5 PD1
#define LCD_4 PD0

#include "lcd.h"
// CONSTANTS
#define WINS 5
#define NOT_THROWN 0
#define ROCK 1
#define PAPER 2
#define SCISSORS 4
#define READING_DELAY 2000

// VARIABLES
unsigned char top_buffer[17];
unsigned char bot_buffer[17];
unsigned char game_status[8];
unsigned char playerA_status[8];
unsigned char playerB_status[8];
unsigned int scoreA = 0;
unsigned int scoreB = 0;
unsigned int playA = NOT_THROWN;
unsigned int playB = NOT_THROWN;

void update_screen() {
    sprintf(top_buffer, "%i   %s   %i ", scoreA, game_status, scoreB);
    go_home();
    write_str(top_buffer);
    sprintf(bot_buffer, "%s  %s", playerA_status, playerB_status);
    go_to(1,0);
    write_str(bot_buffer);
}
void main () {
  // Setup
  DDRD = 0x0F;
  DDRC = 0x03;

  PORTB = 0x07; // PB2, PB1, PB0 pull-up
  PORTC = 0x10; // PC4 pull-up
  PORTD = 0xE0; // PD7, PD6, PD5 pull-up

  // INITIALIZE THE LCD
  init_4bit_lcd(2);
  clear_display();
  set_display(1,0,0);


  while (1) {
    // Update
    if (scoreA < WINS && scoreB < WINS) {
      // Read player A input
      if(!poll(PIND, PD5)) {
        while(!poll(PIND, PD5)); // Forces on button-up
        playA = ROCK;
      }
      if(!poll(PIND, PD6)) {
        while(!poll(PIND, PD6)); // Forces on button-up
        playA = PAPER;
      }
      if(!poll(PIND, PD7)) {
        while(!poll(PIND, PD7)); // Forces on button-up
        playA = SCISSORS;
      }
      // Read player B input
      if(!poll(PINB, PB0)) {
        while(!poll(PINB, PB0)); // Forces on button-up
        playB = ROCK;
      }
      if(!poll(PINB, PB1)) {
        while(!poll(PINB, PB1)); // Forces on button-up
        playB = PAPER;
      }
      if(!poll(PINB, PB2)) {
        while(!poll(PINB, PB2)); // Forces on button-up
        playB = SCISSORS;
      }

      if (playA != NOT_THROWN && playB != NOT_THROWN) {
        // Update each player's play
        switch(playA) {
          case ROCK:
            sprintf(playerA_status, " Piedra");
            break;
          case PAPER:
            sprintf(playerA_status, "  Papel");
            break;
          case SCISSORS:
            sprintf(playerA_status, "Tijeras");
            break;
        }
        switch(playB) {
          case ROCK:
            sprintf(playerB_status, "Piedra ");
            break;
          case PAPER:
            sprintf(playerB_status, "Papel  ");
            break;
          case SCISSORS:
            sprintf(playerB_status, "Tijeras");
            break;
        }

        // Update game status & scores
        if (playA == playB) {
          sprintf(game_status, " EMPATE");
        }
        else if (  (playA == ROCK && playB == SCISSORS)
                || (playA == PAPER && playB == ROCK)
                || (playA == SCISSORS && playB == PAPER)){
          scoreA++;
          sprintf(game_status, " GANA A");
        } else {
          scoreB++;
          sprintf(game_status, " GANA B");
        }
        update_screen();
        _delay_ms(READING_DELAY);
        // Reset game
        playA = NOT_THROWN;
        playB = NOT_THROWN;
        sprintf(game_status, "A TIRAR");
      } else {
        sprintf(game_status, "A TIRAR");

        if (playA == NOT_THROWN)
          sprintf(playerA_status, "  Falta");
        else
          sprintf(playerA_status, "     OK");

        if (playB == NOT_THROWN)
          sprintf(playerB_status, "Falta  ");
        else
          sprintf(playerB_status, "OK     ");
      }

    } else {
      if (scoreA > scoreB) {
        sprintf(game_status, " GANO A");
      } else {
        sprintf(game_status, " GANO B");
      }
      if (!poll(PINC, PC4)) {
        while(!poll(PINC, PC4)); // Forces on button-up
        scoreA = 0;
        scoreB = 0;
      }
    }
    update_screen();
  }
}
